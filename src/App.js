import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, signin } from "./actions";

function App() {
  const counter = useSelector((state) => state.counter);
  const isLogged = useSelector((state) => state.isLogged);
  const dispatch = useDispatch();

  return (
    <div className='App'>
      Counter: {counter}
      <button onClick={() => dispatch(increment(5))}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      <button onClick={() => dispatch(signin())}>Log In</button>
      {console.log(isLogged)}
      {isLogged ? <h3>Valuable Info</h3> : ""}
    </div>
  );
}

export default App;
